﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    public Transform diffPanel;

    private GameDataManager gamedata;

	// Use this for initialization
	void Start ()
    {
        gamedata = GameObject.FindGameObjectWithTag("GameDataManager").GetComponent<GameDataManager>();
        Debug.Log("search gamedata");

        gamedata.LoadCardType();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Play(GameObject obj)
    {
        obj.SetActive(false);
        diffPanel.gameObject.SetActive(true);
    }

    public void SelectDiff(string diff)
    {
        gamedata.difficultySet = diff;

        SceneManager.LoadScene(1);
    }
}
