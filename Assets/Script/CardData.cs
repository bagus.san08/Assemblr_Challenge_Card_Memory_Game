﻿using System.Collections.Generic;

[System.Serializable]
public class CardData
{
    public List<CustomCardClass> cardType = new List<CustomCardClass>();
}

[System.Serializable]
public class CustomCardClass
{
    public string pictPath;
    public string name;
    public int leftUse = 2;

    public CustomCardClass(string pictPath, string name)
    {
        this.pictPath = pictPath;
        this.name = name;
    }
}

