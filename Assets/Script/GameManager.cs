﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private GameDataManager gameData;

    public Transform cardParent;
    public Transform cardObject;
    public GridLayoutGroup gridLayout;

    [Header("Difficulty Setting")]

    public List<Diff> difficultySet = new List<Diff>();

    [Header("UI Stuff")]
    public Text minText;
    public Text secText;
    public Image cardDoneSprite;
    public Text cardDoneText;
    public GameObject winText;

    [Header("Audio Stuff")]
    public AudioSource musicAudiosource;
    public AudioSource audiosource;
    public AudioClip[] clip;

    private int amountCard = 0;
    private int amountCardType = 0;

    private GameObject card1;
    private GameObject card2;

    private int timercount;
    private int min = 0;
    private int sec = 0;

    private List<int> cardTypeRandomizer;

    [HideInInspector]
    public bool clickable = true;

    // Use this for initialization
    void Start ()
    {
        gameData = GameObject.FindGameObjectWithTag("GameDataManager").GetComponent<GameDataManager>();

        for (int i = 0; i < difficultySet.Count; i++)
        {
            if (difficultySet[i].diffName == gameData.difficultySet)
            {
                amountCard = difficultySet[i].amountCardSpawn;
                gridLayout.constraintCount = difficultySet[i].amountRow;

                break;
            }
        }

        amountCardType = amountCard / 2;

        cardTypeRandomizer = new List<int>();

        int randomizer = 0;

        for (int i = 0; i < amountCardType; i++)
        {
            do
            {
                randomizer = Random.Range(0, gameData.cardData.cardType.Count);
            }
            while (cardTypeRandomizer.Contains(randomizer));

            cardTypeRandomizer.Add(randomizer);
        }

        for (int i = 0; i < amountCard; i++)
        {
            Transform obj = Instantiate(cardObject, cardParent);

            int randCardType = Random.Range(0,amountCardType);

            while (gameData.cardData.cardType[cardTypeRandomizer[randCardType]].leftUse <= 0)
            {
                randCardType = Random.Range(0, amountCardType);
            }

            obj.GetComponent<CardInteraction>().upCardSprite = Resources.Load<Sprite>(gameData.cardData.cardType[cardTypeRandomizer[randCardType]].pictPath);
            obj.name = gameData.cardData.cardType[cardTypeRandomizer[randCardType]].name;

            gameData.cardData.cardType[cardTypeRandomizer[randCardType]].leftUse--;
        }

        StartCoroutine(delayDisableGridLayoutForAestheticReason());

        InvokeRepeating("Timer", 1, 1);

    }

    IEnumerator delayDisableGridLayoutForAestheticReason()
    {
        yield return new WaitForSeconds(0.5f);

        gridLayout.enabled = false;
    }
	
	// Update is called once per frame

    public void CompareImage(GameObject card)
    {
        StartCoroutine(compareImageTimer(card));
    }

    IEnumerator compareImageTimer(GameObject card)
    {
        if (card1 == null)
        {
            card1 = card;
        }
        else
        {
            clickable = false;

            card2 = card;

            yield return new WaitForSeconds(1);

            if (card1.name == card2.name)
            {
                cardDoneSprite.enabled = true;
                cardDoneSprite.sprite = card1.GetComponent<Image>().sprite;
                cardDoneText.text = card1.GetComponent<Image>().name;

                Destroy(card1);
                Destroy(card2);

                amountCard -= 2;

                if(amountCard > 0)
                {
                    audiosource.PlayOneShot(clip[0]);
                }
            }
            else
            {
                card1.GetComponent<CardInteraction>().ResetCard();
                card2.GetComponent<CardInteraction>().ResetCard();

                card1 = null;
                card2 = null;
            }

            clickable = true;
        }

        // game finish
        if (amountCard <= 0)
        {
            StartCoroutine(GameOver());
        }
    }

    public void Timer()
    {
        timercount++;
        sec++;

        if (timercount % 60 == 0)
        {
            min++;
            sec = 0;
        }

        minText.text = min.ToString();
        secText.text = sec.ToString();
    }

    public IEnumerator GameOver()
    {
        musicAudiosource.Stop();
        audiosource.PlayOneShot(clip[1]);
        CancelInvoke();
        gameData.SaveHighScore(timercount);
        winText.SetActive(true);

        yield return new WaitForSeconds(2.5f);

        SceneManager.LoadScene(2);
    }
}

[System.Serializable]
public class Diff
{
    public string diffName;
    public int amountCardSpawn;
    public int amountRow;

    public Diff(string pictPath, int name, int amountRow)
    {
        this.diffName = diffName;
        this.amountCardSpawn = amountCardSpawn;
        this.amountRow = amountRow;
    }
}
