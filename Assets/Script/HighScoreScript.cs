﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HighScoreScript : MonoBehaviour {

    public Text[] highScoreList;
    public Text Title;
    public GameObject newHighScoreOBJ;

    private GameDataManager gamedata;

	// Use this for initialization
	void Start () {
        gamedata = GameObject.FindGameObjectWithTag("GameDataManager").GetComponent<GameDataManager>();

        ShowHighScore(gamedata.difficultySet);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowHighScore(string diff)
    {
        if (gamedata.newHighscore > 0)
        {
            newHighScoreOBJ.gameObject.SetActive(true);
        }

        int min = 0;
        int sec = 0;

        for (int i = 0; i < highScoreList.Length; i++)
        {
            min = 0;
            sec = 0;

            highScoreList[i].text = "" + min.ToString() + ":" + sec.ToString();
        }

        if (diff == "easy")
        {
            Title.text = "<color=#ff0000ff>EASY</color> HIGH SCORE";
            gamedata.playerData.EasyHighScore.Sort();

            for (int i=0; i< gamedata.playerData.EasyHighScore.Count; i++)
            {
                min = gamedata.playerData.EasyHighScore[i] / 60;
                sec = gamedata.playerData.EasyHighScore[i] % 60;

                if (newHighScoreOBJ.activeInHierarchy && gamedata.playerData.EasyHighScore[i] == gamedata.newHighscore)
                {
                    highScoreList[i].text = "<color=#ff0000ff>"+"" + min.ToString() + ":" + sec.ToString()+"</color>";
                }
                else
                {
                    highScoreList[i].text = "" + min.ToString() + ":" + sec.ToString();
                }
            }
        }
        else if (diff == "medium")
        {
            Title.text = "<color=#ff0000ff>MEDIUM</color> HIGH SCORE";
            gamedata.playerData.MediumHighScore.Sort();

            for (int i = 0; i < gamedata.playerData.MediumHighScore.Count; i++)
            {
                min = gamedata.playerData.MediumHighScore[i] / 60;
                sec = gamedata.playerData.MediumHighScore[i] % 60;

                if (newHighScoreOBJ.activeInHierarchy && gamedata.playerData.MediumHighScore[i] == gamedata.newHighscore)
                {
                    highScoreList[i].text = "<color=#ff0000ff>" + "" + min.ToString() + ":" + sec.ToString() + "</color>";
                }
                else
                {
                    highScoreList[i].text = "" + min.ToString() + ":" + sec.ToString();
                }
            }
        }
        else
        {
            Title.text = "<color=#ff0000ff>HARD</color> HIGH SCORE";
            gamedata.playerData.HardHighScore.Sort();

            for (int i = 0; i < gamedata.playerData.HardHighScore.Count; i++)
            {
                min = gamedata.playerData.HardHighScore[i] / 60;
                sec = gamedata.playerData.HardHighScore[i] % 60;

                if (newHighScoreOBJ.activeInHierarchy && gamedata.playerData.HardHighScore[i] == gamedata.newHighscore)
                {
                    highScoreList[i].text = "<color=#ff0000ff>" + "" + min.ToString() + ":" + sec.ToString() + "</color>";
                }
                else
                {
                    highScoreList[i].text = "" + min.ToString() + ":" + sec.ToString();
                }
            }
        }

        gamedata.newHighscore = 0;
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
