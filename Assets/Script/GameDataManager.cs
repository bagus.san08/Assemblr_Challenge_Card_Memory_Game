﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using System.Linq;
public class GameDataManager : MonoBehaviour
{
    private string playerFilePath = "PlayerDataJson.json";
    private string cardFilePath = "CardDataJson.json";

    public PlayerData playerData = new PlayerData();
    public CardData cardData = new CardData();

    public static GameDataManager instance = null;

    public string difficultySet = "easy";

    public int newHighscore = 0;

    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        LoadData();
    }

    public void LoadData()
    {
        LoadHighScore();
        LoadCardType();
    }

    public void SaveHighScore(int score)
    {
        if (difficultySet == "easy")
        {
            if (playerData.EasyHighScore.Count < 5)
            {
                playerData.EasyHighScore.Add(score);
                newHighscore = score;
            }
            else
            {
                int maxValue = playerData.EasyHighScore.Max();
                int maxIndex = playerData.EasyHighScore.ToList().IndexOf(maxValue);

                if (maxValue >  score)
                {
                    playerData.EasyHighScore.RemoveAt(maxIndex);
                    playerData.EasyHighScore.Add(score);
                    newHighscore = score;
                }

                Debug.Log("max val = " + maxValue + ", di index = " + maxIndex);
            }
        }

        if (difficultySet == "medium")
        {
            if (playerData.MediumHighScore.Count < 5)
            {
                playerData.MediumHighScore.Add(score);
                newHighscore = score;
            }
            else
            {
                int maxValue = playerData.MediumHighScore.Max();
                int maxIndex = playerData.MediumHighScore.ToList().IndexOf(maxValue);

                if (maxValue > score)
                {
                    playerData.MediumHighScore.RemoveAt(maxIndex);
                    playerData.MediumHighScore.Add(score);
                    newHighscore = score;
                }

                Debug.Log("max val = " + maxValue + ", di index = " + maxIndex);
            }
        }

        if (difficultySet == "hard")
        {
            if (playerData.HardHighScore.Count < 5)
            {
                playerData.HardHighScore.Add(score);
                newHighscore = score;
            }
            else
            {
                int maxValue = playerData.HardHighScore.Max();
                int maxIndex = playerData.HardHighScore.ToList().IndexOf(maxValue);

                if (maxValue > score)
                {
                    playerData.HardHighScore.RemoveAt(maxIndex);
                    playerData.HardHighScore.Add(score);
                    newHighscore = score;
                }

                Debug.Log("max val = " + maxValue + ", di index = " + maxIndex);
            }
        }

        JsonWrapper wrapper = new JsonWrapper();
        wrapper.playerData = playerData;
        string content = JsonUtility.ToJson(playerData, true);
        File.WriteAllText(playerFilePath, content);
    }

    public void SaveCardType()
    {
        JsonWrapper wrapper = new JsonWrapper();
        wrapper.cardData = cardData;
        string content = JsonUtility.ToJson(cardData, true);
        File.WriteAllText(cardFilePath, content);
    }

    public void LoadHighScore()
    {
        if (File.Exists(playerFilePath))
        {
            string content = File.ReadAllText(playerFilePath);
            playerData = JsonUtility.FromJson<PlayerData>(content);
        }
    }

    public void LoadCardType()
    {
        if (File.Exists(cardFilePath))
        {
            string content = File.ReadAllText(cardFilePath);
            cardData = JsonUtility.FromJson<CardData>(content);
        }
    }
}