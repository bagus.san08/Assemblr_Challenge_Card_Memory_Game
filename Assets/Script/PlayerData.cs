﻿using System.Collections.Generic;

[System.Serializable]
public class PlayerData
{
    public List<int> EasyHighScore = new List<int>();
    public List<int> MediumHighScore = new List<int>();
    public List<int> HardHighScore = new List<int>();
}
