﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInteraction : MonoBehaviour {

    public Sprite upCardSprite;
    public Sprite downCardSprite;

    private Image ownSprite;
    private GameManager gamemanager;

	// Use this for initialization
	void Start ()
    {
        ownSprite = GetComponent<Image>();
        gamemanager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        if (gamemanager.clickable && ownSprite.sprite == downCardSprite)
        {
            ownSprite.sprite = upCardSprite;

            gamemanager.CompareImage(this.gameObject);
        }
    }

    public void ResetCard()
    {
        ownSprite.sprite = downCardSprite;
    }
}
